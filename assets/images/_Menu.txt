<!DOCTYPE html>
<html>
<head>
	

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>WHat's Cooking</title>

	<!-- Bootstrap css -->
	<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
	<!-- Bootstrap cdn CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<!-- Externl CSS -->
	<link rel="stylesheet" type="text/css" href="../assets/css/style.css">
	<!-- google fonts -->
	<link href="https://fonts.googleapis.com/css?family=Bowlby+One+SC|Luckiest+Guy|Pacifico&display=swap" rel="stylesheet"> 
	<!-- fontawesome -->
	<script src="https://kit.fontawesome.com/1f61ed690d.js" crossorigin="anonymous"></script>

</head>
<body>
	<nav class="navbar navbar-expand-md navbar-dark bg-danger fixed-top ">

		<div class="mx-auto order-0">
			<a class="navbar-brand mx-auto pl-5" href="../index.html"><span><i class="fas fa-utensils navspan"></i></span> <span class="luto">   Luto ni </span><span class="lola">Lola</span></a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target=".dual-collapse2">
				<span class="navbar-toggler-icon"></span>
			</button>
		</div>
		<div class="navbar-collapse collapse w-100 order-3 dual-collapse2">
			<ul class="navbar-nav ml-auto">
				<li class="nav-item">
					<a class="nav-link " href="index.html">What's cooking</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="../AboutUs/index.html">About us</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#">Location</a>
				</li>
				<li class="nav-item">
					<a class="nav-link pr-5" href="../ContactUs/index.html">Contact Us</a>
				</li>
			</ul>
		</div>
	</nav>
	<!-- end of nav bar -->

<div class="container-fluid">
	<div class="row row-whats row-whats-upper ">
		<!-- fixed column -->
		<div class=" whats-col text-center whats-col-upper fixed">
			<h1 class="whats-h1"> What's Cooking?</h1>
			<span class="text-white download">Download our full menu </span>
			<a href="" download="./Menu.txt"><span class="d-link">Here</span></a>
		</div>

		<div class="col-md-5 whats-col text-cente whats-col-upper empty">
			<!-- empty column to push the next column -->
		</div>

		<div class="col-md-7 whats-col text-cente whats-col-upper">
			<div class="row row-inner">
				<div class="col-md-4">
					<div class="card text-center">
						<img src="../assets/images/chickenadobo2.jpg" class="card-img-top menu-img" alt="...">
						<div class="card-body">
							<h5 class="card-title"><span class="menu">Chicken Adobo</span></h5>
							<p class="price"> <strong>&#8369;</strong></p>
							<p class="menu-description">Adobo refers to a method of marinating and stewing for any cut of meat or fish in a briny mixture of vinegar, soy sauce, and spices. Filipino adobo should not be confused with the spicy Spanish adobo sauce. Although they both share the Spanish name, they are vastly different in flavor and ingredients.</p>
							
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="card text-center">
						<img src="../assets/images/beefKaldereta.jpg" class="card-img-top menu-img" alt="...">
						<div class="card-body">
							<h5 class="card-title"><span class="menu">Beef Kaldereta</span></h5>
							<p class="price"> <strong>&#8369;350</strong></p>
							<p class="menu-description">Adobo refers to a method of marinating and stewing for any cut of meat or fish in a briny mixture of vinegar, soy sauce, and spices. Filipino adobo should not be confused with the spicy Spanish adobo sauce. Although they both share the Spanish name, they are vastly different in flavor and ingredients.</p>
							
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="card text-center">
						<img src="../assets/images/karekare.jpg" class="card-img-top menu-img" alt="...">
						<div class="card-body">
							<h5 class="card-title"><span class="menu">Kare-kare</span></h5>
							<p class="price"> <strong>&#8369;300</strong></p>
							<p class="menu-description">Adobo refers to a method of marinating and stewing for any cut of meat or fish in a briny mixture of vinegar, soy sauce, and spices. Filipino adobo should not be confused with the spicy Spanish adobo sauce. Although they both share the Spanish name, they are vastly different in flavor and ingredients.</p>
							
						</div>
					</div>
				</div>
			</div>	
				<!-- end of inner row -->			
		</div>
	</div>
	<!-- end of row -->
	<div class="row row-whats row-whats-mid ">
		<div class="col-md-5 whats-col text-cente whats-col-mid empty">
			<!-- empty column to push the next column -->
		</div>
		<div class="col-md-7 whats-col text-cente whats-col-mid">
			<div class="row row-inner">
				<div class="col-md-4">
					<div class="card text-center">
						<img src="../assets/images/crispy.jpg" class="card-img-top menu-img" alt="...">
						<div class="card-body">
							<h5 class="card-title"><span class="menu">Crispy Pata</span></h5>
							<p class="price"> <strong>&#8369;320</strong></p>
							<p class="menu-description">Adobo refers to a method of marinating and stewing for any cut of meat or fish in a briny mixture of vinegar, soy sauce, and spices. Filipino adobo should not be confused with the spicy Spanish adobo sauce. Although they both share the Spanish name, they are vastly different in flavor and ingredients.</p>
							
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="card text-center">
						<img src="../assets/images/sinigang.jpg" class="card-img-top menu-img" alt="...">
						<div class="card-body">
							<h5 class="card-title"><span class="menu">Sinigang</span></h5>
							<p class="price"> <strong>&#8369;350</strong></p>
							<p class="menu-description">Adobo refers to a method of marinating and stewing for any cut of meat or fish in a briny mixture of vinegar, soy sauce, and spices. Filipino adobo should not be confused with the spicy Spanish adobo sauce. Although they both share the Spanish name, they are vastly different in flavor and ingredients.</p>
							
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="card text-center">
						<img src="../assets/images/sisig.jpg" class="card-img-top menu-img" alt="...">
						<div class="card-body">
							<h5 class="card-title"><span class="menu">Sisig</span></h5>
							<p class="price"> <strong>&#8369;200</strong></p>
							<p class="menu-description">Adobo refers to a method of marinating and stewing for any cut of meat or fish in a briny mixture of vinegar, soy sauce, and spices. Filipino adobo should not be confused with the spicy Spanish adobo sauce. Although they both share the Spanish name, they are vastly different in flavor and ingredients.</p>
							
						</div>
					</div>
				</div>
			</div>
			<!-- end of inner row -->
		</div>
	</div>
	<!-- end of row -->
</div>
<!-- end of container -->

	<footer class="bg-danger text text-center pt-4 text-white fixed-bottom">
		<div>
			<p class="fp"> &#169;  Luto ni Lola, Inc.</p>
		</div>
	</footer>


	<!-- Bootstrap dependencies -->
	<!-- Jquery library -->
	<script type="text/javascript" src="assets/js/jquery-3.4.1.min.js" ></script>
	<!-- Popper js -->
	<script type="text/javascript" src="assets/js/popper.min.js" ></script>	
	<!-- Bootsrap min js -->
	<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
</body>
</html>